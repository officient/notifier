<?php

namespace Officient\Notifier\Bus;

use Officient\Notifier\Exception\ConnectionException;
use Officient\Notifier\Exception\InvalidResponseException;
use Officient\Notifier\Exception\NotificationException;
use Officient\Notifier\Exception\ResponseDecodeException;

/**
 * Interface BusInterface
 * @package Officient\Notifier\Bus
 */
interface BusInterface
{
    /**
     * @param string $query
     * @param array $postFields
     * @return array
     * @throws ConnectionException
     * @throws InvalidResponseException
     * @throws NotificationException
     * @throws ResponseDecodeException
     */
    public function dispatch(string $query, array $postFields): array;

    /**
     * @return bool
     */
    public function ping(): bool;
}