<?php

namespace Officient\Notifier\Bus;

use Officient\Notifier\Exception\ConnectionException;
use Officient\Notifier\Exception\InvalidResponseException;
use Officient\Notifier\Exception\NotificationException;
use Officient\Notifier\Exception\ResponseDecodeException;

/**
 * Class ApiBus
 * @package Officient\Notifier\Bus
 */
class ApiBus implements BusInterface
{
    /**
     * @var string|null
     */
    private $host;

    /**
     * @var string|null
     */
    private $port;

    /**
     * @var string|null
     */
    private $ownerToken;

    /**
     * Notifier constructor.
     * @param string|null $host
     * @param string|null $port
     * @param string|null $ownerToken
     * @throws ConnectionException
     */
    public function __construct(?string $host, ?string $port, ?string $ownerToken)
    {
        if(substr($host, -1) !== '/') {
            $host .= '/';
        }
        $this->host = $host;
        $this->port = $port;
        $this->ownerToken = $ownerToken;

        if(empty($this->host)) {
            throw new ConnectionException('Host can not be empty');
        }
        if(empty($this->port)) {
            throw new ConnectionException('Port can not be empty');
        }
        if(empty($this->ownerToken)) {
            throw new ConnectionException('Owner token can not be empty');
        }
    }

    /**
     * @inheritDoc
     * @param string $query
     * @param array $postFields
     * @return array
     * @throws ConnectionException
     * @throws InvalidResponseException
     * @throws NotificationException
     * @throws ResponseDecodeException
     */
    public function dispatch(string $query, array $postFields): array
    {
        $handle = curl_init();
        $curlOptionArray = [
            CURLOPT_URL             => $this->host.$query,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_POST            => true,
            CURLOPT_RETURNTRANSFER  => true
        ];

        //We don't set the port if the port is not defined (used in dev)
        if($this->port !== -1 && $this->port !== '' && $this->port !== null) {
            curl_setopt($handle, CURLOPT_PORT, $this->port);
        }

        curl_setopt_array($handle, $curlOptionArray);

        if(empty($postFields['owner_token'])) {
            $postFields['owner_token'] = $this->ownerToken;
        }
        curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($postFields));

        $content = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        if($httpCode === 0) {
            throw new ConnectionException("Could not connect to notification service");
        }

        $response = json_decode($content, true);
        if(!is_array($response)) {
            throw new ResponseDecodeException("Could not decode response");
        }

        if($httpCode !== 200) {
            if(array_key_exists('code', $response) && array_key_exists('message', $response) && array_key_exists('result', $response)) {
                throw new NotificationException($response['message'], $httpCode, $response['result']);
            } else {
                throw new InvalidResponseException('Invalid response: '.$content);
            }
        }

        return $response;
    }

    /**
     * @inheritDoc
     */
    public function ping(): bool
    {
        $handle = curl_init();
        curl_setopt_array($handle, [
            CURLOPT_URL             => $this->host,
            CURLOPT_PORT            => $this->port,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_POST            => true,
            CURLOPT_RETURNTRANSFER  => true
        ]);

        curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);
        return $httpCode > 0;
    }
}