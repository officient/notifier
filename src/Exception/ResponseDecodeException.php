<?php

namespace Officient\Notifier\Exception;

/**
 * Class ResponseDecodeException
 * @package Officient\Notifier\Exception
 */
class ResponseDecodeException extends NotificationException
{

}