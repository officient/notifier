<?php

namespace Officient\Notifier\Exception;

/**
 * Class InvalidResponseException
 * @package Officient\Notifier\Exception
 */
class InvalidResponseException extends NotificationException
{

}