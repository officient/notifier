<?php

namespace Officient\Notifier\Exception;

use Throwable;

/**
 * Class NotificationException
 * @package Officient\Notifier\Exception
 */
class NotificationException extends \Exception
{
    public function __construct($message = "", $code = 0, ?array $payload = null, Throwable $previous = null)
    {
        $message .= ", $code";
        if($payload) {
            $message .= " (".print_r($payload, true).")";
        }
        parent::__construct($message, $code, $previous);
    }
}