<?php

namespace Officient\Notifier\Exception;

/**
 * Class ConnectionException
 * @package Officient\Notifier\Exception
 */
class ConnectionException extends NotificationException
{

}