<?php

namespace Officient\Notifier\Repository;

use Officient\Notifier\Interfaces\EmailStatusRepositoryInterface;
use Officient\Notifier\Model\Email;
use Officient\Notifier\Model\EmailStatus;

class EmailStatusRepository extends AbstractRepository implements EmailStatusRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function countBy(Email $email, ?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $postFields = [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ];

        $response = $this->bus->dispatch('emails/'.$email->getId().'/statuses/count_by', $postFields);
        return $response['result'];
    }

    /**
     * @inheritDoc
     */
    public function findBy(Email $email, ?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $postFields = [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ];

        $response = $this->bus->dispatch('emails/'.$email->getId().'/statuses/find_by', $postFields);
        $statuses = array();
        foreach ($response['result'] as $record) {
            $createdDatetime = null;
            if($record['createdDatetime']) {
                $createdDatetime = new \DateTime($record['createdDatetime']['date'], new \DateTimeZone($record['createdDatetime']['timezone']));
            }

            $statuses[] = (new EmailStatus())
                ->setId($record['id'])
                ->setStatus($record['status'])
                ->setCreatedDatetime($createdDatetime);
        }
        return $statuses;
    }
}