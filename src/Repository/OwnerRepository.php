<?php

namespace Officient\Notifier\Repository;

use Officient\Notifier\Exception\NotificationException;
use Officient\Notifier\Interfaces\OwnerRepositoryInterface;
use Officient\Notifier\Model\Owner;

/**
 * Class OwnerRepository
 * @package Officient\Notifier
 */
class OwnerRepository extends AbstractRepository implements OwnerRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $postFields = [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ];

        $response = $this->bus->dispatch('owners/count_by', $postFields);
        return $response['result'];
    }

    /**
     * @inheritDoc
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $postFields = [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ];

        $response = $this->bus->dispatch('owners/find_by', $postFields);
        $owners = array();
        foreach ($response['result'] as $record) {
            $owners[] = (new Owner())
                ->setId($record['id'])
                ->setName($record['name'])
                ->setToken($record['token'])
                ->setCreatedDatetime(new \DateTime($record['createdDatetime']['date'], new \DateTimeZone($record['createdDatetime']['timezone'])));
        }
        return $owners;
    }

    /**
     * @inheritDoc
     * @throws NotificationException
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?Owner
    {
        $postFields = [
            'criteria' => $criteria,
            'order_by' => $orderBy
        ];

        try {
            $response = $this->bus->dispatch('owners/find_one_by', $postFields);
            $record = $response['result'];
            return (new Owner())
                ->setId($record['id'])
                ->setName($record['name'])
                ->setToken($record['token'])
                ->setCreatedDatetime(new \DateTime($record['createdDatetime']['date'], new \DateTimeZone($record['createdDatetime']['timezone'])));
        } catch (NotificationException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }
}