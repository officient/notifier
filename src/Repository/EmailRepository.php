<?php

namespace Officient\Notifier\Repository;

use Officient\Notifier\Exception\NotificationException;
use Officient\Notifier\Interfaces\EmailRepositoryInterface;
use Officient\Notifier\Model\Email;

/**
 * Class EmailRepository
 * @package Officient\Notifier
 */
class EmailRepository extends AbstractRepository implements EmailRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $postFields = [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ];

        $response = $this->bus->dispatch('emails/count_by', $postFields);
        return $response['result'];
    }

    /**
     * @inheritDoc
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $postFields = [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ];

        $response = $this->bus->dispatch('emails/find_by', $postFields);
        $emails = array();
        foreach ($response['result'] as $record) {
            $emails[] = (new Email())
                ->setId($record['id'])
                ->setMailgunDomain($record['mailgunDomain'])
                ->setIgnoreUserPolicies($record['ignoreUserPolicies'])
                ->setSenderAddress($record['senderAddress'])
                ->setSenderName($record['senderName'])
                ->setRecipientAddress($record['recipientAddress'])
                ->setRecipientName($record['recipientName'])
                ->setReplyToAddress($record['replyToAddress'])
                ->setReplyToName($record['replyToName'])
                ->setSubject($record['subject'])
                ->setText($record['text'])
                ->setHtml($record['html'])
                ->setStatus($record['status'])
                ->setTemplate($record['template'])
                ->setLocale($record['locale'])
                ->setUserVariables($record['userVariables'])
                ->setCallbackUrl($record['callbackUrl'])
                ->setTrack($record['track'])
                ->setDeliveryTries($record['deliveryTries'])
                ->setRequireTls($record['requireTls'])
                ->setOwnerId($record['ownerId'])
                ->setCreatedDatetime(new \DateTime($record['createdDatetime']['date'], new \DateTimeZone($record['createdDatetime']['timezone'])))
                ->setUpdatedDatetime(new \DateTime($record['updatedDatetime']['date'], new \DateTimeZone($record['updatedDatetime']['timezone'])));
        }
        return $emails;
    }

    /**
     * @inheritDoc
     * @throws NotificationException
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?Email
    {
        $postFields = [
            'criteria' => $criteria,
            'order_by' => $orderBy
        ];

        try {
            $response = $this->bus->dispatch('emails/find_one_by', $postFields);
            $record = $response['result'];
            return (new Email())
                ->setId($record['id'])
                ->setMailgunDomain($record['mailgunDomain'])
                ->setIgnoreUserPolicies($record['ignoreUserPolicies'])
                ->setSenderAddress($record['senderAddress'])
                ->setSenderName($record['senderName'])
                ->setRecipientAddress($record['recipientAddress'])
                ->setRecipientName($record['recipientName'])
                ->setReplyToAddress($record['replyToAddress'])
                ->setReplyToName($record['replyToName'])
                ->setSubject($record['subject'])
                ->setText($record['text'])
                ->setHtml($record['html'])
                ->setStatus($record['status'])
                ->setTemplate($record['template'])
                ->setLocale($record['locale'])
                ->setUserVariables($record['userVariables'])
                ->setCallbackUrl($record['callbackUrl'])
                ->setTrack($record['track'])
                ->setDeliveryTries($record['deliveryTries'])
                ->setRequireTls($record['requireTls'])
                ->setOwnerId($record['ownerId'])
                ->setCreatedDatetime(new \DateTime($record['createdDatetime']['date'], new \DateTimeZone($record['createdDatetime']['timezone'])))
                ->setUpdatedDatetime(new \DateTime($record['updatedDatetime']['date'], new \DateTimeZone($record['updatedDatetime']['timezone'])));
        } catch (NotificationException $e) {
            if($e->getCode() === 404) {
                return null;
            }
            throw $e;
        }
    }
}