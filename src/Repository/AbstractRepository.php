<?php


namespace Officient\Notifier\Repository;


use Officient\Notifier\Bus\BusInterface;

/**
 * Class AbstractRepository
 * @package Officient\Notifier
 */
abstract class AbstractRepository
{
    /**
     * @var BusInterface
     */
    protected $bus;

    /**
     * Notifier constructor.
     * @param BusInterface $bus
     */
    public function __construct(BusInterface $bus)
    {
        $this->bus = $bus;
    }
}