<?php

namespace Officient\Notifier\Repository;

use Officient\Notifier\Interfaces\EmailEventRepositoryInterface;
use Officient\Notifier\Model\Email;
use Officient\Notifier\Model\EmailEvent;

class EmailEventRepository extends AbstractRepository implements EmailEventRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function countBy(Email $email, ?array $criteria = null, ?int $limit = null, ?int $offset = null): int
    {
        $postFields = [
            'criteria' => $criteria,
            'limit' => $limit,
            'offset' => $offset
        ];

        $response = $this->bus->dispatch('emails/'.$email->getId().'/events/count_by', $postFields);
        return $response['result'];
    }

    /**
     * @inheritDoc
     */
    public function findBy(Email $email, ?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array
    {
        $postFields = [
            'criteria' => $criteria,
            'order_by' => $orderBy,
            'limit' => $limit,
            'offset' => $offset
        ];

        $response = $this->bus->dispatch('emails/'.$email->getId().'/events/find_by', $postFields);
        $events = array();
        foreach ($response['result'] as $record) {
            $receivedDatetime = null;
            if($record['receivedDatetime']) {
                $receivedDatetime = new \DateTime($record['receivedDatetime']['date'], new \DateTimeZone($record['receivedDatetime']['timezone']));
            }
            $createdDatetime = null;
            if($record['createdDatetime']) {
                $createdDatetime = new \DateTime($record['createdDatetime']['date'], new \DateTimeZone($record['createdDatetime']['timezone']));
            }

            $events[] = (new EmailEvent())
                ->setId($record['id'])
                ->setType($record['type'])
                ->setSubtype($record['subtype'])
                ->setDetails($record['details'])
                ->setOrigin($record['origin'])
                ->setOriginal($record['original'])
                ->setReceivedDatetime($receivedDatetime)
                ->setCreatedDatetime($createdDatetime);
        }
        return $events;
    }
}