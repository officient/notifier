<?php

namespace Officient\Notifier\Repository;

use Officient\Notifier\Interfaces\EmailAttachmentRepositoryInterface;
use Officient\Notifier\Model\Email;
use Officient\Notifier\Model\EmailAttachment;

class EmailAttachmentRepository extends AbstractRepository implements EmailAttachmentRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function findAll(Email $email): array
    {
        $response = $this->bus->dispatch('emails/'.$email->getId().'/attachments/find_all', []);
        $attachments = array();
        foreach ($response['result'] as $record) {
            $createdDatetime = null;
            if($record['createdDatetime']) {
                $createdDatetime = new \DateTime($record['createdDatetime']['date'], new \DateTimeZone($record['createdDatetime']['timezone']));
            }
            $updatedDatetime = null;
            if($record['updatedDatetime']) {
                $updatedDatetime = new \DateTime($record['updatedDatetime']['date'], new \DateTimeZone($record['updatedDatetime']['timezone']));
            }

            $attachments[] = (new EmailAttachment())
                ->setId($record['id'])
                ->setName($record['originalName'])
                ->setMimeType($record['mimeType'])
                ->setSize($record['size'])
                ->setCreatedDatetime($createdDatetime)
                ->setUpdatedDatetime($updatedDatetime);
        }
        return $attachments;
    }

    /**
     * @inheritDoc
     */
    public function download(Email $email, EmailAttachment $emailAttachment): string
    {
        $response = $this->bus->dispatch('emails/'.$email->getId().'/attachments/'.$emailAttachment->getId().'/download', []);
        return $response['result']['base64_data'];
    }
}