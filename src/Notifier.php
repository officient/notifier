<?php

namespace Officient\Notifier;

use DateTime;
use DateTimeZone;
use Officient\Notifier\Bus\BusInterface;
use Officient\Notifier\Exception\ConnectionException;
use Officient\Notifier\Exception\InvalidResponseException;
use Officient\Notifier\Exception\NotificationException;
use Officient\Notifier\Exception\ResponseDecodeException;
use Officient\Notifier\Interfaces\NotifierInterface;
use Officient\Notifier\Model\Email;
use Officient\Notifier\Model\EmailAttachment;

/**
 * Class Notifier
 * @package Officient\Notifier
 */
class Notifier implements NotifierInterface
{
    /**
     * @var BusInterface
     */
    private $bus;

    /**
     * @var string
     */
    private $mailgunDomain;

    /**
     * Notifier constructor.
     * @param BusInterface $bus
     * @param string $mailgunDomain
     */
    public function __construct(BusInterface $bus, string $mailgunDomain)
    {
        $this->bus = $bus;
        $this->mailgunDomain = $mailgunDomain;
    }

    /**
     * @param Email $email
     * @return Email
     * @throws ConnectionException
     * @throws InvalidResponseException
     * @throws NotificationException
     * @throws ResponseDecodeException
     */
    public function sendEmail(Email $email): Email
    {
        if(empty($email->getMailgunDomain())) {
            $email->setMailgunDomain($this->mailgunDomain);
        }
        $attachments = array_map(function(EmailAttachment $emailAttachment) {
            return [
                'name' => $emailAttachment->getName(),
                'mime_type' => $emailAttachment->getMimeType(),
                'base64_data'=> $emailAttachment->getBase64Data()
            ];
        }, $email->getAttachments());
        $postFields = [
            'mailgun_domain' => $email->getMailgunDomain(),
            'ignore_user_policies' => intval($email->getIgnoreUserPolicies()),
            'subject' => $email->getSubject(),
            'text' => $email->getText(),
            'html' => $email->getHtml(),
            'template' => $email->getTemplate(),
            'locale' => $email->getLocale(),
            'sender_name' => $email->getSenderName(),
            'sender_address' => $email->getSenderAddress(),
            'recipient_name' => $email->getRecipientName(),
            'recipient_address' => $email->getRecipientAddress(),
            'extra_recipients' => $email->getExtraRecipients(),
            'reply_to_name' => $email->getReplyToName(),
            'reply_to_address' => $email->getReplyToAddress(),
            'cc' => $email->getCc(),
            'bcc' => $email->getBcc(),
            'attachments' => $attachments,
            'user_variables' => $email->getUserVariables(),
            'callback_url' => $email->getCallbackUrl(),
            'require_tls' => $email->getRequireTls(),
            'owner_token' => $email->getOwnerToken()
        ];
        $response = $this->bus->dispatch('emails/send', $postFields);
        $record = $response['result'];
        return (new Email())
            ->setId($record['id'])
            ->setMailgunDomain($record['mailgunDomain'])
            ->setIgnoreUserPolicies($record['ignoreUserPolicies'])
            ->setSenderAddress($record['senderAddress'])
            ->setSenderName($record['senderName'])
            ->setRecipientAddress($record['recipientAddress'])
            ->setRecipientName($record['recipientName'])
            ->setReplyToAddress($record['replyToAddress'])
            ->setReplyToName($record['replyToName'])
            ->setSubject($record['subject'])
            ->setText($record['text'])
            ->setHtml($record['html'])
            ->setStatus($record['status'])
            ->setTemplate($record['template'])
            ->setLocale($record['locale'])
            ->setUserVariables($record['userVariables'])
            ->setCallbackUrl($record['callbackUrl'])
            ->setTrack($record['track'])
            ->setDeliveryTries($record['deliveryTries'])
            ->setRequireTls($record['requireTls'])
            ->setCreatedDatetime(new DateTime($record['createdDatetime']['date'], new DateTimeZone($record['createdDatetime']['timezone'])))
            ->setUpdatedDatetime(new DateTime($record['updatedDatetime']['date'], new DateTimeZone($record['updatedDatetime']['timezone'])));
    }

    /**
     * @param Email $email
     * @param string|null $recipientName
     * @param string|null $recipientAddress
     * @param bool $withCc
     * @param bool $withBcc
     * @param bool $withAttachments
     * @param bool $withExtraRecipients
     * @throws ConnectionException
     * @throws InvalidResponseException
     * @throws NotificationException
     * @throws ResponseDecodeException
     */
    public function sendEmailAgain(Email $email, ?string $recipientName = null, ?string $recipientAddress = null, bool $withCc = true, bool $withBcc = true, bool $withAttachments = true, bool $withExtraRecipients = true)
    {
        $postFields = [
            'recipient_name' => $recipientName,
            'recipient_address' => $recipientAddress,
            'with_cc' => $withCc,
            'with_bcc' => $withBcc,
            'with_extra_recipients' => $withExtraRecipients,
            'with_attachments' => $withAttachments
        ];

        $this->bus->dispatch('emails/'.$email->getId().'/send_again', $postFields);
    }
}