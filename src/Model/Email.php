<?php

namespace Officient\Notifier\Model;

/**
 * Class Email
 * @package Officient\Notifier
 */
class Email implements \JsonSerializable
{
    const LOCALE_EN     = 'de';
    const LOCALE_DA     = 'da';
    const LOCALE_DE     = 'de';
    const LOCALE_NO     = 'no';

    const TEMPLATE_EFACTO_DEFAULT   = 'efacto.default';

    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $mailgunDomain;

    /**
     * @var bool
     */
    protected $ignoreUserPolicies = false;

    /**
     * @var string
     */
    protected $subject;

    /**
     * @var string|null
     */
    protected $text;

    /**
     * @var string|null
     */
    protected $html;

    /**
     * @var string|null
     */
    protected $status;

    /**
     * @var string|null
     */
    protected $template;

    /**
     * @var string|null
     */
    protected $locale;

    /**
     * @var string|null
     */
    protected $senderName;

    /**
     * @var string|null
     */
    protected $senderAddress;

    /**
     * @var string|null
     */
    protected $recipientName;

    /**
     * @var string|null
     */
    protected $recipientAddress;

    /**
     * @var Address[]|null
     */
    protected $extraRecipients;

    /**
     * @var string|null
     */
    protected $replyToName;

    /**
     * @var string|null
     */
    protected $replyToAddress;

    /**
     * @var Address[]|null
     */
    protected $cc;

    /**
     * @var Address[]|null
     */
    protected $bcc;

    /**
     * @var array|null
     */
    protected $userVariables;

    /**
     * @var array|null
     */
    protected $attachments;

    /**
     * @var string|null
     *
     */
    protected $callbackUrl;

    /**
     * @var string|null
     */
    protected $track;

    /**
     * @var int|null
     */
    protected $deliveryTries;

    /**
     * @var bool|null
     */
    protected $requireTls;

    /**
     * @var string|null
     */
    protected $ownerToken;

    /**
     * @var int|null
     */
    protected $ownerId;

    /**
     * @var \DateTime|null
     */
    protected $createdDatetime;

    /**
     * @var \DateTime|null
     */
    protected $updatedDatetime;

    public function __construct()
    {
        $this->attachments = array();
        $this->userVariables = array();
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Email
     */
    public function setId(?int $id): Email
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMailgunDomain(): ?string
    {
        return $this->mailgunDomain;
    }

    /**
     * @param string|null $mailgunDomain
     * @return Email
     */
    public function setMailgunDomain(?string $mailgunDomain): Email
    {
        $this->mailgunDomain = $mailgunDomain;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIgnoreUserPolicies(): bool
    {
        return $this->ignoreUserPolicies;
    }

    /**
     * @param bool $ignoreUserPolicies
     * @return Email
     */
    public function setIgnoreUserPolicies(bool $ignoreUserPolicies): Email
    {
        $this->ignoreUserPolicies = $ignoreUserPolicies;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @param string|null $subject
     * @return Email
     */
    public function setSubject(?string $subject): Email
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     * @return Email
     */
    public function setText(?string $text): Email
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHtml(): ?string
    {
        return $this->html;
    }

    /**
     * @param string|null $html
     * @return Email
     */
    public function setHtml(?string $html): Email
    {
        $this->html = $html;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return Email
     */
    public function setStatus(?string $status): Email
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemplate(): ?string
    {
        return $this->template;
    }

    /**
     * @param string|null $template
     * @return Email
     */
    public function setTemplate(?string $template): Email
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     * @return Email
     */
    public function setLocale(?string $locale): Email
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderName(): ?string
    {
        return $this->senderName;
    }

    /**
     * @param string|null $senderName
     * @return Email
     */
    public function setSenderName(?string $senderName): Email
    {
        $this->senderName = $senderName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSenderAddress(): ?string
    {
        return $this->senderAddress;
    }

    /**
     * @param string|null $senderAddress
     * @return Email
     */
    public function setSenderAddress(?string $senderAddress): Email
    {
        $this->senderAddress = $senderAddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientName(): ?string
    {
        return $this->recipientName;
    }

    /**
     * @param string|null $recipientName
     * @return Email
     */
    public function setRecipientName(?string $recipientName): Email
    {
        $this->recipientName = $recipientName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientAddress(): ?string
    {
        return $this->recipientAddress;
    }

    /**
     * @param string|null $recipientAddress
     * @return Email
     */
    public function setRecipientAddress(?string $recipientAddress): Email
    {
        $this->recipientAddress = $recipientAddress;
        return $this;
    }

    /**
     * @return Address[]|null
     */
    public function getExtraRecipients(): ?array
    {
        return $this->extraRecipients;
    }

    /**
     * @param Address[] $extraRecipients
     * @return Email
     */
    public function setExtraRecipients(array $extraRecipients): Email
    {
        $this->extraRecipients = $extraRecipients;
        return $this;
    }

    public function addExtraRecipient(Address $extraRecipient): Email
    {
        if(!is_array($this->extraRecipients)) {
            $this->extraRecipients = array();
        }
        $this->extraRecipients[] = $extraRecipient;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReplyToName(): ?string
    {
        return $this->replyToName;
    }

    /**
     * @param string|null $replyToName
     * @return Email
     */
    public function setReplyToName(?string $replyToName): Email
    {
        $this->replyToName = $replyToName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReplyToAddress(): ?string
    {
        return $this->replyToAddress;
    }

    /**
     * @param string|null $replyToAddress
     * @return Email
     */
    public function setReplyToAddress(?string $replyToAddress): Email
    {
        $this->replyToAddress = $replyToAddress;
        return $this;
    }

    /**
     * @return Address[]|null
     */
    public function getCc(): ?array
    {
        return $this->cc;
    }

    /**
     * @param Address[]|null $cc
     * @return $this
     */
    public function setCc(?array $cc): Email
    {
        $this->cc = $cc;
        return $this;
    }

    /**
     * @param Address $cc
     * @return $this
     */
    public function addCc(Address $cc): Email
    {
        if(!is_array($this->cc)) {
            $this->cc = array();
        }
        $this->cc[] = $cc;
        return $this;
    }

    /**
     * @return Address[]|null
     */
    public function getBcc(): ?array
    {
        return $this->bcc;
    }

    /**
     * @param Address[]|null $bcc
     * @return $this
     */
    public function setBcc(?array $bcc): Email
    {
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * @param Address $bcc
     * @return $this
     */
    public function addBcc(Address $bcc): Email
    {
        if(!is_array($this->bcc)) {
            $this->bcc = array();
        }
        $this->bcc[] = $bcc;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getUserVariables(): ?array
    {
        return $this->userVariables;
    }

    /**
     * @param array|null $userVariables
     * @return Email
     */
    public function setUserVariables(?array $userVariables): Email
    {
        $this->userVariables = $userVariables;
        return $this;
    }

    /**
     * @param string $key
     * @param string $value
     * @return Email
     */
    public function addUserVariable(string $key, string $value): Email
    {
        if(!is_array($this->userVariables)) {
            $this->userVariables = array();
        }
        $this->userVariables[$key] = $value;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAttachments(): ?array
    {
        return $this->attachments;
    }

    /**
     * @param array|null $attachments
     * @return Email
     */
    public function setAttachments(?array $attachments): Email
    {
        $this->attachments = $attachments;
        return $this;
    }

    /**
     * @param EmailAttachment $attachment
     * @return $this
     */
    public function addAttachment(EmailAttachment $attachment): Email
    {
        if(!is_array($this->attachments)) {
            $this->attachments = array();
        }
        $this->attachments[] = $attachment;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCallbackUrl(): ?string
    {
        return $this->callbackUrl;
    }

    /**
     * @param string|null $callbackUrl
     * @return Email
     */
    public function setCallbackUrl(?string $callbackUrl): Email
    {
        $this->callbackUrl = $callbackUrl;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTrack(): ?string
    {
        return $this->track;
    }

    /**
     * @param string|null $track
     * @return Email
     */
    public function setTrack(?string $track): Email
    {
        $this->track = $track;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDeliveryTries(): ?int
    {
        return $this->deliveryTries;
    }

    /**
     * @param int|null $deliveryTries
     * @return Email
     */
    public function setDeliveryTries(?int $deliveryTries): Email
    {
        $this->deliveryTries = $deliveryTries;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRequireTls(): ?bool
    {
        return $this->requireTls;
    }

    /**
     * @param bool|null $requireTls
     * @return Email
     */
    public function setRequireTls(?bool $requireTls): Email
    {
        $this->requireTls = $requireTls;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOwnerToken(): ?string
    {
        return $this->ownerToken;
    }

    /**
     * @param string|null $ownerToken
     * @return Email
     */
    public function setOwnerToken(?string $ownerToken): Email
    {
        $this->ownerToken = $ownerToken;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    /**
     * @param int|null $ownerId
     * @return Email
     */
    public function setOwnerId(?int $ownerId): Email
    {
        $this->ownerId = $ownerId;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDatetime(): ?\DateTime
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTime|null $createdDatetime
     * @return Email
     */
    public function setCreatedDatetime(?\DateTime $createdDatetime): Email
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedDatetime(): ?\DateTime
    {
        return $this->updatedDatetime;
    }

    /**
     * @param \DateTime|null $updatedDatetime
     * @return Email
     */
    public function setUpdatedDatetime(?\DateTime $updatedDatetime): Email
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }
}