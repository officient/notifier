<?php

namespace Officient\Notifier\Model;

/**
 * Class Owner
 * @package Officient\Notifier
 */
class Owner implements \JsonSerializable
{
    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $token;

    /**
     * @var \DateTime|null
     */
    protected $createdDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Owner
     */
    public function setId(?int $id): Owner
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Owner
     */
    public function setName(?string $name): Owner
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     * @return Owner
     */
    public function setToken(?string $token): Owner
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDatetime(): ?\DateTime
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTime|null $createdDatetime
     * @return Owner
     */
    public function setCreatedDatetime(?\DateTime $createdDatetime): Owner
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }
}