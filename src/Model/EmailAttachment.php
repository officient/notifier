<?php

namespace Officient\Notifier\Model;

/**
 * Class EmailAttachment
 * @package Officient\Notifier
 */
class EmailAttachment implements \JsonSerializable
{
    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $mimeType;

    /**
     * @var int|null
     */
    protected $size;

    /**
     * @var string|null
     */
    protected $base64Data;

    /**
     * @var string|null
     */
    protected $data;

    /**
     * @var \DateTime|null
     */
    protected $createdDatetime;

    /**
     * @var \DateTime|null
     */
    protected $updatedDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return EmailAttachment
     */
    public function setId(?int $id): EmailAttachment
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return EmailAttachment
     */
    public function setName(?string $name): EmailAttachment
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param string|null $mimeType
     * @return EmailAttachment
     */
    public function setMimeType(?string $mimeType): EmailAttachment
    {
        $this->mimeType = $mimeType;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param int|null $size
     * @return EmailAttachment
     */
    public function setSize(?int $size): EmailAttachment
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBase64Data(): ?string
    {
        return $this->base64Data;
    }

    /**
     * @param string|null $base64Data
     * @return EmailAttachment
     */
    public function setBase64Data(?string $base64Data): EmailAttachment
    {
        $this->base64Data = $base64Data;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getData(): ?string
    {
        return $this->data;
    }

    /**
     * @param string|null $data
     * @return EmailAttachment
     */
    public function setData(?string $data): EmailAttachment
    {
        $this->data = $data;
        $this->base64Data = base64_encode($data);
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDatetime(): ?\DateTime
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTime|null $createdDatetime
     * @return EmailAttachment
     */
    public function setCreatedDatetime(?\DateTime $createdDatetime): EmailAttachment
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdatedDatetime(): ?\DateTime
    {
        return $this->updatedDatetime;
    }

    /**
     * @param \DateTime|null $updatedDatetime
     * @return EmailAttachment
     */
    public function setUpdatedDatetime(?\DateTime $updatedDatetime): EmailAttachment
    {
        $this->updatedDatetime = $updatedDatetime;
        return $this;
    }
}