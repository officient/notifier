<?php

namespace Officient\Notifier\Model;

/**
 * Class Address
 * @package Officient\Notifier
 */
class Address implements \JsonSerializable
{
    /**
     * @var string
     */
    private $address;

    /**
     * @var string|null
     */
    private $name;

    /**
     * Address constructor.
     * @param string $address
     * @param string $name
     */
    public function __construct(string $address, string $name = '')
    {
        $this->address = $address;
        $this->name = $name;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        if(empty($this->name)) {
            return [
                'address' => $this->address
            ];
        } else {
            return [
                'name' => $this->name,
                'address' => $this->address
            ];
        }
    }
}