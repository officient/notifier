<?php

namespace Officient\Notifier\Model;

class EmailStatus implements \JsonSerializable
{
    const SUBMITTED_FOR_DELIVERY        = 'submitted_for_delivery';
    const SUBMIT_FOR_DELIVERY_FAILED    = 'submit_for_delivery_failed';
    const DELIVERED                     = 'delivered';
    const OPENED                        = 'opened';
    const CLICKED                       = 'clicked';
    const COMPLAINED                    = 'complained';
    const FAILED                        = 'failed';
    const UNSUBSCRIBED                  = 'unsubscribed';

    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $status;

    /**
     * @var \DateTime|null
     */
    protected $createdDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return EmailStatus
     */
    public function setId(?int $id): EmailStatus
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     * @return EmailStatus
     */
    public function setStatus(?string $status): EmailStatus
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDatetime(): ?\DateTime
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTime|null $createdDatetime
     * @return EmailStatus
     */
    public function setCreatedDatetime(?\DateTime $createdDatetime): EmailStatus
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }
}