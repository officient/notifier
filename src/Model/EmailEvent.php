<?php

namespace Officient\Notifier\Model;


class EmailEvent implements \JsonSerializable
{
    const TYPE_SUBMITTED_FOR_DELIVERY       = 'submitted_for_delivery';
    const TYPE_SUBMIT_FOR_DELIVERY_FAILED   = 'submit_for_delivery_failed';
    const TYPE_DELIVERED                    = 'delivered';
    const TYPE_OPENED                       = 'opened';
    const TYPE_CLICKED                      = 'clicked';
    const TYPE_COMPLAINED                   = 'complained';
    const TYPE_TEMPORARY_FAILED             = 'temporary_failed';
    const TYPE_PERMANENTLY_FAILED           = 'permanently_failed';
    const TYPE_UNSUBSCRIBED                 = 'unsubscribed';
    const TYPE_BLOCKED                      = 'blocked';

    const SUBTYPE_BLOCKED_POLICY_BLOCKED    = 'policy_blocked';
    const SUBTYPE_BLOCKED_ATTACHMENT_LIMIT  = 'attachment_limit';

    const ORIGIN_INTERNAL                   = 'internal';
    const ORIGIN_EXTERNAL_MAILGUN           = 'external_mailgun';

    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $type;

    /**
     * @var string|null
     */
    protected $subtype;

    /**
     * @var string|null
     */
    protected $details;

    /**
     * @var string|null
     */
    protected $origin;

    /**
     * @var array|null
     */
    protected $original;

    /**
     * @var \DateTime|null
     */
    protected $receivedDatetime;

    /**
     * @var \DateTime|null
     */
    protected $createdDatetime;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return EmailEvent
     */
    public function setId(?int $id): EmailEvent
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return EmailEvent
     */
    public function setType(?string $type): EmailEvent
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubtype(): ?string
    {
        return $this->subtype;
    }

    /**
     * @param string|null $subtype
     * @return EmailEvent
     */
    public function setSubtype(?string $subtype): EmailEvent
    {
        $this->subtype = $subtype;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDetails(): ?string
    {
        return $this->details;
    }

    /**
     * @param string|null $details
     * @return EmailEvent
     */
    public function setDetails(?string $details): EmailEvent
    {
        $this->details = $details;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrigin(): ?string
    {
        return $this->origin;
    }

    /**
     * @param string|null $origin
     * @return EmailEvent
     */
    public function setOrigin(?string $origin): EmailEvent
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getOriginal(): ?array
    {
        return $this->original;
    }

    /**
     * @param array|null $original
     * @return EmailEvent
     */
    public function setOriginal(?array $original): EmailEvent
    {
        $this->original = $original;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getReceivedDatetime(): ?\DateTime
    {
        return $this->receivedDatetime;
    }

    /**
     * @param \DateTime|null $receivedDatetime
     * @return EmailEvent
     */
    public function setReceivedDatetime(?\DateTime $receivedDatetime): EmailEvent
    {
        $this->receivedDatetime = $receivedDatetime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedDatetime(): ?\DateTime
    {
        return $this->createdDatetime;
    }

    /**
     * @param \DateTime|null $createdDatetime
     * @return EmailEvent
     */
    public function setCreatedDatetime(?\DateTime $createdDatetime): EmailEvent
    {
        $this->createdDatetime = $createdDatetime;
        return $this;
    }
}