<?php

namespace Officient\Notifier\Interfaces;

use Officient\Notifier\Model\Email;

/**
 * Interface NotifierInterface
 * @package Officient\Notifier
 */
interface NotifierInterface
{
    /**
     * @param Email $email
     * @return Email
     */
    public function sendEmail(Email $email): Email;

    /**
     * @param Email $email
     * @param string|null $recipientName
     * @param string|null $recipientAddress
     * @param bool $withCc
     * @param bool $withBcc
     * @param bool $withAttachments
     * @param bool $withExtraRecipients
     * @return void
     */
    public function sendEmailAgain(Email $email, ?string $recipientName = null, ?string $recipientAddress = null, bool $withCc = true, bool $withBcc = true, bool $withAttachments = true, bool $withExtraRecipients = true);
}