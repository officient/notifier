<?php

namespace Officient\Notifier\Interfaces;

use Officient\Notifier\Model\Email;
use Officient\Notifier\Model\EmailAttachment;

/**
 * Interface EmailAttachmentRepositoryInterface
 * @package Officient\Notifier
 */
interface EmailAttachmentRepositoryInterface
{
    /**
     * @param Email $email
     * @return array
     */
    public function findAll(Email $email): array;

    /**
     * @param EmailAttachment $emailAttachment
     * @return string
     */
    public function download(Email $email, EmailAttachment $emailAttachment): string;
}