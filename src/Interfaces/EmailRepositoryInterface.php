<?php

namespace Officient\Notifier\Interfaces;

use Officient\Notifier\Model\Email;

/**
 * Interface EmailRepositoryInterface
 * @package Officient\Notifier
 */
interface EmailRepositoryInterface
{
    /**
     * @param array|null $criteria
     * @param int|null $limit
     * @param int|null $offset
     * @return int
     */
    public function countBy(?array $criteria = null, ?int $limit = null, ?int $offset = null): int;

    /**
     * @param array|null $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function findBy(?array $criteria = null, ?array $orderBy = null, ?int $limit = null, ?int $offset = null): array;

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @return Email|null
     */
    public function findOneBy(array $criteria, ?array $orderBy = null): ?Email;
}